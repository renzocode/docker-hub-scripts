f = open("nodejs-url.sh", "r").read()
sp = f.split("/")
v = sp[5].split("-")

dis= v[2]
architecture = v[3].split(".")[0]

version = v[1]
distro = dis + "-" + architecture
nodejsPath = '/usr/local/lib/nodejs'


w = open("nodejs.sh", "w")
w.write("wget " + f)

mk = open('nodejs.sh', 'a')
mk.write('mkdir -p /usr/local/lib/nodejs\n')
mk.write('tar -xJvf node-'+ version + '-' + distro + '.tar.xz -C /usr/local/lib/nodejs\n')
mk.write('echo "export PATH=/usr/local/lib/nodejs/node-{0}-{1}/bin:$PATH" >> /root/.profile\n'.format(version, distro))

symlink = open('symlink.sh', "w")

symlink.write('ln -s {0}/node-{1}-{2}/bin/node /usr/bin/node\n'.format(nodejsPath, version, distro))
symlink.write('ln -s {0}/node-{1}-{2}/bin/npm /usr/bin/npm\n'.format(nodejsPath, version, distro))
symlink.write('ln -s {0}/node-{1}-{2}/bin/npx /usr/bin/npx\n'.format(nodejsPath, version, distro))
symlink.write('npm i -g @nrwl/cli\n')
symlink.write('ln -s {0}/node-{1}-{2}/bin/nx /usr/bin/nx\n'.format(nodejsPath, version, distro))

