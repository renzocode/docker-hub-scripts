wget https://nodejs.org/dist/v12.18.4/node-v12.18.4-linux-x64.tar.xz
mkdir -p /usr/local/lib/nodejs
tar -xJvf node-v12.18.4-linux-x64.tar.xz -C /usr/local/lib/nodejs
echo "export PATH=/usr/local/lib/nodejs/node-v12.18.4-linux-x64/bin:$PATH" >> /root/.profile
